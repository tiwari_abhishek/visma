from visma.functions.operator import Binary
from visma.functions.structure import Expression
from visma.functions.constant import Constant
from visma.simplify.simplify import simplify, simplifyEquation
from visma.matrix.structure import Matrix, SquareMat
from visma.gui import logger



def gauss_elim(mat):
    """
    Returns calculated values of unknown variables

    Arguments:
        mat {visma.matrix.structure.Matrix} -- matrix token

    Returns:
        result {visma.matrix.structure.Matrix} -- result matrix token
    
    Note: result is a Nx1 matrix
    """
    echelon = Matrix()
    echelon = row_echelon(mat)
    result = Matrix()
    result.empty([mat.dim[0], 1])

    N = mat.dim[0]
    M = mat.dim[1]
    index = N-1
    for i in range(N-1,-1,-1):
        sum_ = []
        temp = []
        if echelon.value[i][i][0].value==0.0:        # no unique solution for this case
            return -1
        for j in range(i+1, M-1):
            temp=[]
            temp.extend(echelon.value[i][j])
            temp.append(Binary('*'))
            temp.extend(result.value[j][0])
            temp,_,_,_,_ = simplify(temp)
            sum_.extend(temp) 
            if j!=M-2:
                sum_.append(Binary('+'))
        sum_,_,_,_,_ = simplify(sum_)
        result.value[index][0].extend(echelon.value[i][M-1])
        if sum_:
            if sum_[0].value<0:
                result.value[index][0].append(Binary('+'))
            else:
                result.value[index][0].append(Binary('-'))
            result.value[index][0].extend(sum_)
        
        result.value[index][0],_,_,_,_ = simplify(result.value[index][0])
        result.value[index][0].append(Binary('/'))
        result.value[index][0].extend(echelon.value[i][i])
        result.value[index][0],_,_,_,_ = simplify(result.value[index][0])
        index -= 1

    return result