from visma.functions.operator import Binary
from visma.functions.structure import Expression
from visma.functions.constant import Constant
from visma.simplify.simplify import simplify, simplifyEquation
from visma.matrix.structure import Matrix, SquareMat
from visma.gui import logger



def row_echelon(mat):
    """
    Returns the row echelon form of the given matrix

    Arguments:
        mat {visma.matrix.structure.Matrix} -- matrix token

    Returns:
        row_echelon {visma.matrix.structure.Matrix} -- result matrix token
    
    """
    N = mat.dim[0]
    M = mat.dim[1]
    for k in range(0,N):
        
        if abs(mat.value[k][k][0].value)==0.0:
            if k==N-1:
                return simplifyMatrix(mat)
            else:
                for i in range(0,N):
                    t = mat.value[k][i]
                    mat.value[k][i] = mat.value[k+1][i]
                    mat.value[k+1][i] = t
        else:
            for i in range(k+1,N):
                temp=[]
                temp.extend(mat.value[i][k])
                temp.append(Binary('/'))
                temp.extend(mat.value[k][k])
                temp,_,_,_,_ = simplify(temp)
                for j in range(k+1,M):
                    temp2 = []
                    temp2.extend(mat.value[k][j])
                    temp2.append(Binary('*'))
                    temp2.extend(temp)
                    temp2,_,_,_,_ = simplify(temp2)
                    mat.value[i][j].append(Binary('-'))
                    mat.value[i][j].extend(temp2)
                mat.value[i][k].append(Binary('*'))
                mat.value[i][k].append(Constant(0))
                mat = simplifyMatrix(mat)
    return simplifyMatrix(mat)