import subprocess, os, signal
from visma.gui import logger

commands = [
    'set title {title}',
    'set xlabel {xlabel}',
    'set ylabel {ylabel}',
    "set terminal dumb;"
]
def cli_plot(eqn):
    """Renders the graph plot for the equation obtained

    Arguments:
        eqn {String}: result equation

    Returns:
        None

    P.S:
    This is just a demo.
    This function will include working with tokens in the future.
    
    """

    term = "dumb" 
    # Ask user if he wants a popup matplotlib window?
    # if yes then term = 'qt'
    if len(eqn)==0:
        print("No plot available for this output")
        logger.error("No plot available for the output")
    else:
        plot = "set terminal {}; plot "+eqn+" ;".format(term)
        gnuplot = subprocess.Popen(["/usr/bin/gnuplot"], 
                                    persist=0,
                                    stdin=subprocess.PIPE,shell=True, preexec_fn=os.setsid) 
        gnuplot.stdin.write (bytes(plot+"exit;","utf-8"))
        gnuplot.stdin.write (bytes("exit\n","utf-8"))
        gnuplot.stdin.flush()
        gnuplot.wait()

        #Kill the process just as a precaution
        #otherwise if there is an error in gnuplot which is not caught
        #the result may be stored in buffer and wont display plots next time
        os.killpg(os.getpgid(gnuplot.pid), signal.SIGTERM) 

