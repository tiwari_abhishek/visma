import os, sys
from visma.gui.window import initGUI
from visma.gui.cli import commandExec
from visma.gui import logger
from prompt_toolkit import prompt
from prompt_toolkit.history import FileHistory
from prompt_toolkit.auto_suggest import AutoSuggestFromHistory
from prompt_toolkit.completion import Completer, Completion
import click
from fuzzyfinder import fuzzyfinder
from prompt_toolkit.key_binding import KeyBindings
from prettytable import PrettyTable 
from prompt_toolkit.validation import Validator, ValidationError
from visma.io.tokenize import tokenizer, getLHSandRHS, normalize, tokenizeSymbols, removeSpaces, getTerms, removeUnary
from visma.io.checks import checkEquation
from prompt_toolkit import print_formatted_text, HTML
from prompt_toolkit.formatted_text import FormattedText


VismaKeys = ['simplify', 'differentiate', 'factorize', 'integrate','find roots','help',
            'manual', 'gui']

class Visma(Completer):
    def get_completions(self, document, complete_event):
        word_before_cursor = document.get_word_before_cursor(WORD=True)
        matches = fuzzyfinder(word_before_cursor, VismaKeys)
        for m in matches:
            yield Completion(m, start_position=-len(word_before_cursor))

class CommandValidator(Validator):
    def validate(self, document):
        res = False
        text = document.text
        decimal = 0
        specialChars = ['@','_','%','$']

        for i in text:
            if i in specialChars:
                raise ValidationError(message='This input contains special characters not supported by VisMA')
            if i!=' ':
                if i=='.':
                    decimal+=1
            else:
                decimal = 0
            if decimal>1:
                raise ValidationError(message='Decimals cannot contain more than one decimal point.')
            
            ########################
            # Real time Validation #
            ########################

            if '(' in text:
                inputEquation = text.split('(', 1)[1][:-1]
                cleanEqn = removeSpaces(inputEquation)
                terms = getTerms(cleanEqn)
                normalizedTerms = normalize(terms)
                symTokens = tokenizeSymbols(normalizedTerms)
                normalizedTerms, symTokens = removeUnary(normalizedTerms, symTokens)
                
                if checkEquation(normalizedTerms, symTokens)==True:
                    pass
                else:
                    raise ValidationError(message="Invalid Expression.")

def init():
    sys.stdout.flush()
    x = PrettyTable()
    x.field_names = ['command', 'arguments', 'function']
    x.add_row(['gui', 'None', 'opens up gui for VisMa'])
    x.add_row(['exit', 'None', 'closes the prompt'])
    x.add_row(['simplify', 'equation or expression', 'Simplifies the given equation.'])
    x.add_row(['addition', 'equation or expression', 'Adds the elements used. '])
    x.add_row(['subtraction', 'equation or expression', 'Subtracts the elements used'])
    x.add_row(['multiplication', 'equation or expression', 'Multiplies the elements used.'])
    x.add_row(['','',''])
    x.add_row(['division', 'equation or expression', 'Divides the elements used. '])
    x.add_row(['factorize', 'equation or expression', 'Factorizes the expression.'])
    x.add_row(['find-roots', 'equation or expression', 'Solves the quadratic equation for the variable in the equation.'])
    x.add_row(['','',''])
    x.add_row(['solve', 'equation or expression', 'Solves the equation for the given variable.'])
    x.add_row(['integrate', 'expression , variable', 'Integrates the expression by the given variable'])
    x.add_row(['differentiate', 'expression , variable', 'Differentiates the expression by the given variable. '])
    open(os.path.abspath("log.txt"), "w").close()
    logger.setLevel(10)
    logger.setLogName('main')
    logger.info('Initialising VisMa...(currently in CLI mode)')
    intro = "Welcome to the command line interface of VisMa.\ntype \"help\" to open the user manual\n\n"
    print(intro)
    text = FormattedText([
        ('#FF3ACC', 'VisMa > ')
        ])

    while(1):
        try:
            cin = prompt(text,
                            history=FileHistory('visma/gui/history.txt'),
                            auto_suggest=AutoSuggestFromHistory(),
                            completer=Visma(),
                            complete_while_typing=True,
                            validator=CommandValidator(),
                            )
        except EOFError:
            break
        except:
            cin='exit'
            print("Exiting VisMa....")
            break
        if cin == 'gui':
            initGUI()
        elif 'help' in cin:
            if 'manual' in cin:
                print(x)

        else:
            try:
                commandExec(cin)
            except Exception:
                logger.error("Invalid Expression: %s ", cin)
                print("Invalid Expression")
    if (cin == 'exit'):
        logger.info('Exiting VisMa...')


if __name__ == '__main__':
    init()
